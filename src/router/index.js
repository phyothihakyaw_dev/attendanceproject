import Vue from 'vue'
import Router from 'vue-router'
import AddPeople from '@/components/AddPeople'
import Login from '@/components/Login'
import AuthGuard from './auth-guard'
import AttendPeople from '@/components/AttendPeople'
import SearchAttendance from '@/components/SearchAttendance'
import Print from '@/components/Print'
import Rules from '@/components/Rules'
import Payroll from '@/components/Payroll'
import HRNote from '@/components/HRNote'

Vue.use(Router)

export default new Router({
  mode : 'history',
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/add',
      name: 'AddPeople',
      component: AddPeople,
      beforeEnter : AuthGuard,
      meta: {
        title : 'Add People'
      }
    },
    {
      path: '/attend',
      name: 'AttendPeople',
      component: AttendPeople,
      beforeEnter : AuthGuard,
      meta: {
        title: 'Attend People'
      }
    },
    {
      path: '/search',
      name: 'SearchAttendance',
      component: SearchAttendance,
      beforeEnter: AuthGuard,
      meta: {
        title: 'Search Attendance'
      }
    },
    {
      path: '/print',
      name: 'Print',
      component: Print,
      beforeEnter: AuthGuard
    },
    {
      path: '/rules',
      name: 'Rules',
      component: Rules,
      beforeEnter: AuthGuard,
      meta: {
        title: 'Rules'
      }
    },
    {
      path: '/payroll',
      name: 'Payroll',
      component: Payroll,
      beforeEnter: AuthGuard,
      meta: {
        title: 'Payroll'
      }
    },
    {
      path: '/note',
      name: 'HRNote',
      component: HRNote,
      beforeEnter: AuthGuard,
      meta : {
        title: 'HR Note'
      }
    }
  ]
})
