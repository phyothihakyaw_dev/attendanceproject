import Vue from 'vue'
import Vuex from 'vuex'
import * as firebase from 'firebase'
import router from '../router'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state : {
        admin : null,
        employeeList : [],
        employeeNames : [],
        pushID : null,
        todayData : [],
        searchResult : [],
        leaveEmployee: '',
        remaingLeaveCount: null,
        hrRules : null,
        allEmployee : [],
        duplicateData : '',
        nameHolder: [],
        salaryResult : [],
        HRNote: []
    },
    actions : {
        
        //admin signin
        adminSignin ({commit}, payload) {
            var email = payload.email
            var password = payload.password
            firebase.auth().signInWithEmailAndPassword(email,password)
            .then(
                router.push('/add')
            )
            .catch(function(error) {
                var errorCode = error.code
                var errorMessage = error.message
                if (errorCode === 'auth/wrong-password') {
                    alert('Wrong password')
                } else {
                    alert (errorMessage)
                } 
                console.log(error)
            })
        },

        //auto signin
        autoSignin ({commit, getters, state}, payload) {
            commit('setAdmin', {
                id : payload.uid
            })
            router.push('/add')
        },

        //admin logout
        adminLogout ({commit}) {
            firebase.auth().signOut()
            commit('setAdmin', null)
        },

        //submit employee data to firebase
        submitEmployeeData ({commit, getters, state}, payload) {
            const employeeData = {
                id : payload.id,
                name : payload.name,
                position : payload.position,
                phone : payload.phone,
                email : payload.email,
                salary : payload.salary,
                remainingLeaves : payload.remainingLeaves
            }
            firebase.database().ref('Employees').push(employeeData)
        },

        //load employee data from firebase
        loadEmployeeData ({commit, getters, state}, payload) {
            firebase.database().ref('Employees').on('value', data => {
                const totalEmployee = data.val()
                const arrayOfEmployeeData = []
                for (let key in totalEmployee ) {
                    arrayOfEmployeeData.push({
                        firebaseKey : key,
                        id : totalEmployee[key].id,
                        name : totalEmployee[key].name,
                        position : totalEmployee[key].position,
                        phone : totalEmployee[key].phone,
                        email : totalEmployee[key].email
                    })
                }
                commit('employeeList', arrayOfEmployeeData)
            })
        },

        //upload edited employee data to firebase
        saveEditedEmployeeData ({commit, getters, state}, payload) {
            firebase.database().ref('Employees/').child(payload.firebaseKey)
                .update({
                    id : payload.id,
                    name : payload.name,
                    position : payload.position,
                    phone : payload.phone,
                    email : payload.email
                })
                
        },

        //load employee names for select box
        loadEmployeeNames ({commit, getters, state}, payload) {
            firebase.database().ref('Employees').on('value', data => {
                const employeeNames = []
                const employeeObj = data.val()
                for ( let key in employeeObj) {
                    employeeNames.push({
                        name : employeeObj[key].name
                    })
                }
                commit('setEmployeeNames', employeeNames)
            })
        },

        //submit presence data to firebase
        submitPresenceAttendanceData ({commit, getters, state}, payload) {
            var boolForButton = null
            if(payload.leaveNote == '-'){
                boolForButton = false
            }else{
                boolForButton = true
            }
            const presenceDataObject = {
                attendStatus : payload.attendStatus,
                arriveTime : payload.arriveTime,
                leaveTime : payload.leaveTime,
                date : payload.date,
                name : payload.employee,
                leaveNote : payload.leaveNote,
                boolForButton : boolForButton,
                percent : payload.percent
            }
            firebase.database().ref('Attendance').child(payload.date).
            child(payload.employee).set(presenceDataObject)
        },

        //submit leave data to firebase
        submitLeaveAttendanceData ({commit, getters, state}, payload) {
            var boolForButton = null
            if(payload.leaveNote == '-'){
                boolForButton = false
            }else{
                boolForButton = true
            }
            const leaveDataObject = {
                attendStatus : payload.attendStatus,
                leaveNote : payload.leaveNote,
                boolForButton : boolForButton,
                date : payload.date,
                name : payload.employee,
                arriveTime : payload.arriveTime,
                leaveTime : payload.leaveTime,
                leaveReason : payload.leaveReason,
                percent : payload.percent
            }
            firebase.database().ref('Attendance').child(payload.date).
            child(payload.employee).set(leaveDataObject)
        },

        //submit absence data to firebase
        submitAbsenceAttendanceData ({commit, getters, state}, payload) {
            var boolForButton = null
            if(payload.leaveNote == '-'){
                boolForButton = false
            }else{
                boolForButton = true
            }
            const absenceDataObject = {
                attendStatus : payload.attendStatus,
                date : payload.date,
                name : payload.employee,
                arriveTime : payload.arriveTime,
                leaveTime : payload.leaveTime,
                leaveNote : payload.leaveNote,
                boolForButton : boolForButton,
                percent : payload.percent
            }
            firebase.database().ref('Attendance').child(payload.date).
            child(payload.employee).set(absenceDataObject)
        },

        //load attendance data for today from firebase
        loadTodayAttendance ({commit, getters, state}, payload) {
            var today = new Date()
            var dd = today.getDate()
            var mm = today.getMonth()+1 
            var yyyy = today.getFullYear()

            if(dd<10) {
            dd = '0'+dd
            } 

            if(mm<10) {
            mm = '0'+mm
            } 
            today = yyyy + '-' + mm + '-' + dd
            firebase.database().ref('Attendance').child(today).on("value", data => {
                let obj = data.val()
                const todayDataArray = []
                for ( let key in obj ) {
                    if(obj[key].attendStatus == 'Presence') {
                        todayDataArray.push({
                            arriveTime : obj[key].arriveTime,
                            attendStatus : obj[key].attendStatus,
                            // date : obj[key].date,
                            leaveTime : obj[key].leaveTime,
                            name : obj[key].name,
                            leaveNote : '-',
                            boolForButton: obj[key].boolForButton,
                            percent: obj[key].percent
                        })
                        commit('setTodayData', todayDataArray)
                    } else if (obj[key].attendStatus == 'Leave') {
                        todayDataArray.push({
                            attendStatus : obj[key].attendStatus,
                            // date : obj[key].date,
                            leaveNote : obj[key].leaveNote,
                            name : obj[key].name,
                            arriveTime : '-',
                            leaveTime : '-',
                            leaveReason: obj[key].leaveReason,
                            boolForButton: obj[key].boolForButton,
                            percent: obj[key].percent
                        })
                        commit('setTodayData', todayDataArray)
                    } else {
                        todayDataArray.push({
                            attendStatus : obj[key].attendStatus,
                            // date : obj[key].date,
                            name : obj[key].name,
                            leaveNote : '-',
                            arriveTime : '-',
                            leaveTime : '-',
                            boolForButton: obj[key].boolForButton,
                            percent: obj[key].percent
                        })
                        commit('setTodayData', todayDataArray)
                    }
                } 
            })
        },

        //load attendace search result from firebase
        searchAttendance({commit, getters, state}, payload) {
            firebase.database().ref('Attendance').orderByKey().
            startAt(payload.startDate).endAt(payload.endDate).on('value', data => {
              const obj = data.val()
              const objResult = []
              const resultArray = []
              for ( let key in obj) {
                  for (let i in obj[key]) {
                      objResult.push(obj[key][i])
                  }
              }
              for ( let i = 0 ; i < objResult.length ; i++ ) {
                  if  (objResult[i].name == payload.searchedEmployee) {
                      resultArray.push(objResult[i])
                  }
              }
              commit('setSearchResult', resultArray)
            })
        },

        //submit salary data to firebase
        submitSalary ({commit, getters, state}, payload) {
            const salaryObject = {
                employee : payload.employee,
                salary : payload.salary,
                remainingLeaves : payload.remainingLeaves
            }
            firebase.database().ref('Salary').child(payload.employee).set(salaryObject)
        },

        //submit rules to firebase
        submitRules ({commit, getters, state}, payload) {
            const rulesObject = {
                personalLeave : payload.personalLeave,
                medicalLeave : payload.medicalLeave,
                absenceLeave : payload.absenceLeave,
                detectionFee : payload.detectionFee
            }
            firebase.database().ref('Rules').set(rulesObject)
        },
        
        //submit salary search parameters to firebase
        searchForSalaryByMonth ({commit, getters, state}, payload) {
            const searchName = payload.employee
            const searchResult = []
            firebase.database().ref('Attendance').orderByKey().
            startAt(payload.startDate).endAt(payload.endDate).once('value', data => {
                const salaryResult = data.val()
                for (let key in salaryResult) {
                    if (salaryResult[key].Salary !== undefined) {
                        for (let x in salaryResult[key].Salary) {
                            if (salaryResult[key].Salary[x].name == payload.employee) {
                                searchResult.push({
                                    name : payload.employee,
                                    salary : salaryResult[key].Salary[x].salary,
                                    date : key 
                                })
                            }
                        }
                    }
                }
                console.log(searchResult)
                commit('setSalaryResult', searchResult)
            })
        },

        //load all employees 
        loadEmployees({commit, getters, state}, payload) {
            firebase.database().ref('Employees').on('value', data => {
                const allEmployee = data.val()
                const employeeObjArray = []
                for (let key in allEmployee) {
                    employeeObjArray.push({
                        firebaseKey : key,
                        email: allEmployee[key].email,
                        id : allEmployee[key].id,
                        name : allEmployee[key].name,
                        phone : allEmployee[key].phone,
                        position : allEmployee[key].position,
                        remainingLeaves : allEmployee[key].remainingLeaves,
                        salary : allEmployee[key].salary      
                    })
                }
                commit('setAllEmployee', employeeObjArray)
            })
        },

        //substract remaining leaves from firebase
        substractRemainingLeaveCount ({commit, getters, state}, payload) {
            firebase.database().ref('Employees/' + payload.firebaseKey + '/remainingLeaves').transaction( data => {
                return data - 1
            })
        },
        addRemainingLeaveCount({commit, getters}, payload){
            firebase.database().ref('Employees/' + payload.firebaseKey + '/remainingLeaves').transaction( data => {
                return data + 1
            })
        },

        //check attendance is duplicate or not in firebase
        checkDuplicateAttendance ({commit, getters, state}, payload) {
            firebase.database().ref('Attendance/' + payload.checkDate).on('value', data => {
                const dataToCheck = data.val()
                const keysToCheck = []
                for (let key in dataToCheck) {
                    keysToCheck.push(key)
                }
                commit('setDuplicateChecker', keysToCheck)
            })
        },
        
        //calculate monthly salary
        calculateMonthlySalary ({commit, getters, state}, payload) {
            firebase.database().ref('Attendance').orderByKey().startAt(payload.startDate).endAt(payload.endDate).once('value', data => {
                const queryData = data.val()
                const queryArray = []
                for (let x in queryData) {
                    for (let y in queryData[x]) {
                        queryArray.push(queryData[x][y])
                    }
                }
                firebase.database().ref('Employees').once('value', data => {
                    for(let key in data.val()) {
                        this.state.nameHolder.push(data.val()[key].name)
                    }
                })
                var answerHolder = 0
                const answerArray = []
                var temp = ''
                for (let i in this.state.nameHolder) {
                    for (let j in queryArray) {
                        if (this.state.nameHolder[i] == queryArray[j].name) {
                            answerHolder += Number(queryArray[j].percent)
                        }
                        
                    }
                    temp = this.state.nameHolder[i]
                    answerArray.push({ 
                        name: temp, 
                        percent: answerHolder
                    })
                    temp = ''
                    answerHolder = 0
                }
                var convertPercent = 0
                var salary = 0
                const salaryArray = []
                firebase.database().ref('Employees').once('value', data => {
                    for (let key in data.val()) {
                        for (let x in answerArray) {
                            if (data.val()[key].name == answerArray[x].name) {
                                convertPercent =  data.val()[key].salary * (answerArray[x].percent / 100)
                                salary = Number(data.val()[key].salary) - Number(convertPercent)
                            } 
                        }
                        salaryArray.push({
                            name : data.val()[key].name,
                            salary : salary
                        })
                    }
                })
                firebase.database().ref('Attendance/' + payload.endDate + '/Salary').once('value',data => {
                    if (data.val() == null) {
                        firebase.database().ref('Attendance/' + payload.endDate).child('Salary').set(salaryArray)
                    }
                }) 
            })
        },

        // save hr note to firebase
        saveHRNote ({commit, getters, state}, payload) {
            const noteData = {
                note : payload.note,
                time : payload.time
            }
            firebase.database().ref('Notes').child(payload.date).push(noteData)
        },
        updateNote({commit}, payload){
            firebase.database().ref('Notes').child(payload.date + '/' + payload.id).update({
                note : payload.note,
                time : payload.time
            })
        },
        //search note by date 
        searchNote ({commit, getters, state}, payload) {
            firebase.database().ref('Notes').orderByKey().startAt(payload.startDate).endAt(payload.endDate).once('value', data => {
                const obj = data.val()
                const loadedNoteData = []
                for (let i in obj) {
                    for (let key in obj[i]) {
                        loadedNoteData.push({
                            date : i,
                            key : key,
                            time : obj[i][key].time,
                            note : obj[i][key].note
                        })
                    }
                }
                commit('setHRNote', loadedNoteData)
            })
        },
        loadHrRules({commit, getters}){
            firebase.database().ref('Rules').once('value', data => {
                const obj = data.val()
                const rules = {
                    absenceLeave: obj.absenceLeave,
                    detectionFee: obj.detectionFee,
                    medicalLeave: obj.medicalLeave,
                    personalLeave: obj.personalLeave
                }
                commit('setHRRules', rules)
            })
        }
    },
    mutations : {
        setAdmin (state, payload) {
            state.admin = payload
        },
        employeeList (state, payload) {
            state.employeeList = payload
        },
        setEmployeeNames (state, payload) {
            state.employeeNames = payload
        },
        setTodayData (state, payload) {
            state.todayData = payload
        },
        setSearchResult (state, payload) {
            state.searchResult = payload
        },
        setHRRules (state, payload) {
            state.hrRules = payload
        },
        setAllEmployee (state, payload) {
            state.allEmployee = payload
        },
        setDuplicateChecker (state, payload) {
            state.duplicateData = payload
        },
        setSalaryResult (state, payload) {
            state.salaryResult = payload
        },
        setHRNote (state, payload) {
            state.HRNote = payload
        }
    },
    getters : {
        admin (state) {
            return state.admin
        },
        employeeList (state) {
            return state.employeeList
        },
        employeeNames (state) {
            return state.employeeNames
        },
        todayData (state) {
            return state.todayData
        },
        searchResult (state) {
            return state.searchResult
        },
        hrRules (state) {
            return state.hrRules
        },
        allEmployee (state) {
            return state.allEmployee
        },
        duplicateData (state) {
            return state.duplicateData
        },
        workedHours (state) {
            return state.workedHours
        },
        salaryResult (state) {
            return state.salaryResult
        },
        HRNote (state) {
            return state.HRNote
        }
    }
})