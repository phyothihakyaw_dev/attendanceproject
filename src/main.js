// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import * as firebase from 'firebase'
import { store } from './store'

Vue.use(Vuetify, { theme: {
  primary: '#1565c0',
  secondary: '#424242',
  accent: '#82B1FF',
  error: '#FF5252',
  info: '#2196F3',
  success: '#4CAF50',
  warning: '#FFC107'
}})

Vue.config.productionTip = false

var config = {
  apiKey: "AIzaSyDr0_SIeHVjhZlmLarPzhDr9C5YZ88kTQs",
  authDomain: "my-attendance-proj.firebaseapp.com",
  databaseURL: "https://my-attendance-proj.firebaseio.com",
  projectId: "my-attendance-proj",
  storageBucket: "my-attendance-proj.appspot.com",
  messagingSenderId: "1037206036815"
};
firebase.initializeApp(config);


/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>',
  created () {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSignin', user)
      }
    })
  },
  methods : {
      
    }
  
})
